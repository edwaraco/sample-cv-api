class AuthenticationController < ApplicationController
  skip_before_action :authorize_request, only: :authenticate

  # Create a token for given pair email, password valid
  def authenticate
    auth_token = AuthenticateUser.new(auth_params[:email], auth_params[:password])
    json_response(auth_token: auth_token.call)
  end

  private

  def auth_params
    params.permit(:email, :password)
  end

end
