class User < ApplicationRecord
  # encrypt password
  has_secure_password

  # Validations
  validates_presence_of :full_name, :email, :password_digest, :summary, :presentation_letter
end
