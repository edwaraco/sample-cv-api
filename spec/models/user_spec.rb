require 'rails_helper'

RSpec.describe User, type: :model do

  # Ensure fields
  it { should validate_presence_of(:full_name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_presence_of(:summary) }
  it { should validate_presence_of(:presentation_letter) }

end
