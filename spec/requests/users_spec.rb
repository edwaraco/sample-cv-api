require 'rails_helper'


RSpec.describe 'Users API', type: :request do
  # Building an user to save in our API
  let(:user) { build(:user) }
  # Building allowed headers
  let(:headers) { valid_headers.except('Authorization')}
  # Check the passwrod digest
  let(:valid_attributes) do
    attributes_for(:user, password_confirmation: user.password)
  end
  # User signup test suite
  describe 'POST /signup' do
    context 'A valid request' do
      before { post '/signup', params: valid_attributes.to_json, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(201)
      end

      it 'returns a success message' do
        expect(json['message']).to match(/Account created successfully/)
      end

      it 'returns an authentication token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'An invalid request' do
      before { post '/signup', params: user.to_json, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(422)
      end

      it 'returns a success message' do
        expect(json['message']).to match(/Password can't be blank/)
      end

    end

  end
end
