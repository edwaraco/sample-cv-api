FactoryBot.define do
  factory :user do
    full_name {Faker::Name.name}
    email { 'edward@test.com' }
    password { 'sample_test' }
    summary { 'User testing...' }
    presentation_letter { 'My first sample app with Ruby' }
  end
end
