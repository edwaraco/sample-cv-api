# SAMPLE CV API

Sample project to build a curriculum vitae API.
This project is built to learn [Ruby on Rails](https://rubyonrails.org/).

## Dependencies

- Ruby version 2.5.1
- Rails 5.2.1


## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rspec
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```
