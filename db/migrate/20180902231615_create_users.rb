class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :email
      t.string :password_digest
      t.string :summary
      t.string :presentation_letter

      t.timestamps
    end
  end
end
